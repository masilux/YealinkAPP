# YealinkApp

geschrieben mit Lazarus (Freepascal)

![Hauptform Version 0.0.6](/Screenshots/YealinkAPP_0_0_6.png)

## Downloads
### für Debian
[yealinkapp 0.0.2](https://nc.masilux.de/index.php/s/Hdb3HL4JBS3yC9S) <br>
[yealinkapp 0.0.4](https://nc.masilux.de/index.php/s/PYnMokEjXaeN25z) <br>
[yealinkapp 0.0.5](https://nc.masilux.de/index.php/s/PpAqWgkBcxyJH47) <br>
[yealinkapp 0.0.6](https://nc.masilux.de/index.php/s/A4PtRZp2aJKrM9f) <br>

[Webeite und weitere Infos](https://www.masilux.de/doku.php?id=software:yealinkapp:start)
