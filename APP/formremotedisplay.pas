unit formremotedisplay;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, IpHtml, Ipfilebroker, Forms, Controls, Graphics,
  Dialogs, ExtCtrls, LazHelpHTML, Buttons, StdCtrls;

type

  { TFrmRemoteDisplay }

  TFrmRemoteDisplay = class(TForm)
    BtnHoch: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    Button1: TButton;
    Image1: TImage;
    procedure BtnHochClick(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
  private

  public

  end;

var
  FrmRemoteDisplay: TFrmRemoteDisplay;

implementation

{$R *.lfm}

{ TFrmRemoteDisplay }

procedure TFrmRemoteDisplay.Image1Click(Sender: TObject);
begin
   { TODO : http://192.168.2.147/servlet?command=screenshot  }
end;

procedure TFrmRemoteDisplay.BitBtn4Click(Sender: TObject);
begin

end;

procedure TFrmRemoteDisplay.BtnHochClick(Sender: TObject);
begin

end;

procedure TFrmRemoteDisplay.BitBtn6Click(Sender: TObject);
begin

end;

procedure TFrmRemoteDisplay.Button1Click(Sender: TObject);
begin
  Close;
end;

end.

