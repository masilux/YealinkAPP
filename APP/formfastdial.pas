unit formfastdial;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, RTTICtrls, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons, IniFiles, fphttpclient;

type

  { TFrmFastdial }

  TFrmFastdial = class(TForm)
    BntFastdial1: TButton;
    BntFastdial10: TButton;
    BntFastdial2: TButton;
    BntFastdial3: TButton;
    BntFastdial4: TButton;
    BntFastdial5: TButton;
    BntFastdial6: TButton;
    BntFastdial7: TButton;
    BntFastdial8: TButton;
    BntFastdial9: TButton;
    procedure BntFastdial10Click(Sender: TObject);
    procedure BntFastdial1Click(Sender: TObject);
    procedure BntFastdial2Click(Sender: TObject);
    procedure BntFastdial3Click(Sender: TObject);
    procedure BntFastdial4Click(Sender: TObject);
    procedure BntFastdial5Click(Sender: TObject);
    procedure BntFastdial6Click(Sender: TObject);
    procedure BntFastdial7Click(Sender: TObject);
    procedure BntFastdial8Click(Sender: TObject);
    procedure BntFastdial9Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  FrmFastdial: TFrmFastdial;
  INI:TINIFile;
  User,Pass,IP,Login,url_call: string;

implementation

{$R *.lfm}

{ TFrmFastdial }

procedure TFrmFastdial.FormCreate(Sender: TObject);
begin
  INI := TINIFile.Create('conf/conf.ini');
  try
    BntFastdial1.Caption:=INI.ReadString('speedcall','text1','');
    BntFastdial2.Caption:=INI.ReadString('speedcall','text2','');
    BntFastdial3.Caption:=INI.ReadString('speedcall','text3','');
    BntFastdial4.Caption:=INI.ReadString('speedcall','text4','');
    BntFastdial5.Caption:=INI.ReadString('speedcall','text5','');
    BntFastdial6.Caption:=INI.ReadString('speedcall','text6','');
    BntFastdial7.Caption:=INI.ReadString('speedcall','text7','');
    BntFastdial8.Caption:=INI.ReadString('speedcall','text8','');
    BntFastdial9.Caption:=INI.ReadString('speedcall','text9','');
    BntFastdial10.Caption:=INI.ReadString('speedcall','text10','');
  finally
    Ini.Free;
  end;
end;

procedure TFrmFastdial.BntFastdial1Click(Sender: TObject);
begin
  INI := TINIFile.Create('conf/conf.ini');
    try
      User := INI.ReadString('general','user', '');
      Pass := INI.ReadString('general','pass', '');
      IP := INI.ReadString('general','ip', '');
      Login := User + ':' + Pass + '@' + IP;
      url_call := 'http://' + Login + '/servlet?key=number=' +  INI.ReadString('speedcall','nummer1','') + '&outgoing_uri=' + INI.ReadString('konto1','url','');
      TFPCustomHTTPClient.SimpleGet(url_call);
    finally
      Ini.Free;
    end;
end;

procedure TFrmFastdial.BntFastdial10Click(Sender: TObject);
begin
  INI := TINIFile.Create('conf/conf.ini');
    try
      User := INI.ReadString('general','user', '');
      Pass := INI.ReadString('general','pass', '');
      IP := INI.ReadString('general','ip', '');
      Login := User + ':' + Pass + '@' + IP;
      url_call := 'http://' + Login + '/servlet?key=number=' +  INI.ReadString('speedcall','nummer10','') + '&outgoing_uri=' + INI.ReadString('konto1','url','');
      TFPCustomHTTPClient.SimpleGet(url_call);
    finally
      Ini.Free;
    end;
end;

procedure TFrmFastdial.BntFastdial2Click(Sender: TObject);
begin
  INI := TINIFile.Create('conf/conf.ini');
    try
      User := INI.ReadString('general','user', '');
      Pass := INI.ReadString('general','pass', '');
      IP := INI.ReadString('general','ip', '');
      Login := User + ':' + Pass + '@' + IP;
      url_call := 'http://' + Login + '/servlet?key=number=' +  INI.ReadString('speedcall','nummer2','') + '&outgoing_uri=' + INI.ReadString('konto1','url','');
      TFPCustomHTTPClient.SimpleGet(url_call);
    finally
      Ini.Free;
    end;
end;

procedure TFrmFastdial.BntFastdial3Click(Sender: TObject);
begin
  INI := TINIFile.Create('conf/conf.ini');
    try
      User := INI.ReadString('general','user', '');
      Pass := INI.ReadString('general','pass', '');
      IP := INI.ReadString('general','ip', '');
      Login := User + ':' + Pass + '@' + IP;
      url_call := 'http://' + Login + '/servlet?key=number=' +  INI.ReadString('speedcall','nummer3','') + '&outgoing_uri=' + INI.ReadString('konto1','url','');
      TFPCustomHTTPClient.SimpleGet(url_call);
    finally
      Ini.Free;
    end;
end;

procedure TFrmFastdial.BntFastdial4Click(Sender: TObject);
begin
  INI := TINIFile.Create('conf/conf.ini');
    try
      User := INI.ReadString('general','user', '');
      Pass := INI.ReadString('general','pass', '');
      IP := INI.ReadString('general','ip', '');
      Login := User + ':' + Pass + '@' + IP;
      url_call := 'http://' + Login + '/servlet?key=number=' +  INI.ReadString('speedcall','nummer4','') + '&outgoing_uri=' + INI.ReadString('konto1','url','');
      TFPCustomHTTPClient.SimpleGet(url_call);
    finally
      Ini.Free;
    end;
end;

procedure TFrmFastdial.BntFastdial5Click(Sender: TObject);
begin
  INI := TINIFile.Create('conf/conf.ini');
    try
      User := INI.ReadString('general','user', '');
      Pass := INI.ReadString('general','pass', '');
      IP := INI.ReadString('general','ip', '');
      Login := User + ':' + Pass + '@' + IP;
      url_call := 'http://' + Login + '/servlet?key=number=' +  INI.ReadString('speedcall','nummer5','') + '&outgoing_uri=' + INI.ReadString('konto1','url','');
      TFPCustomHTTPClient.SimpleGet(url_call);
    finally
      Ini.Free;
    end;
end;

procedure TFrmFastdial.BntFastdial6Click(Sender: TObject);
begin
  INI := TINIFile.Create('conf/conf.ini');
    try
      User := INI.ReadString('general','user', '');
      Pass := INI.ReadString('general','pass', '');
      IP := INI.ReadString('general','ip', '');
      Login := User + ':' + Pass + '@' + IP;
      url_call := 'http://' + Login + '/servlet?key=number=' +  INI.ReadString('speedcall','nummer6','') + '&outgoing_uri=' + INI.ReadString('konto1','url','');
      TFPCustomHTTPClient.SimpleGet(url_call);
    finally
      Ini.Free;
    end;
end;

procedure TFrmFastdial.BntFastdial7Click(Sender: TObject);
begin
  INI := TINIFile.Create('conf/conf.ini');
    try
      User := INI.ReadString('general','user', '');
      Pass := INI.ReadString('general','pass', '');
      IP := INI.ReadString('general','ip', '');
      Login := User + ':' + Pass + '@' + IP;
      url_call := 'http://' + Login + '/servlet?key=number=' +  INI.ReadString('speedcall','nummer7','') + '&outgoing_uri=' + INI.ReadString('konto1','url','');
      TFPCustomHTTPClient.SimpleGet(url_call);
    finally
      Ini.Free;
    end;
end;

procedure TFrmFastdial.BntFastdial8Click(Sender: TObject);
begin
  INI := TINIFile.Create('conf/conf.ini');
    try
      User := INI.ReadString('general','user', '');
      Pass := INI.ReadString('general','pass', '');
      IP := INI.ReadString('general','ip', '');
      Login := User + ':' + Pass + '@' + IP;
      url_call := 'http://' + Login + '/servlet?key=number=' +  INI.ReadString('speedcall','nummer8','') + '&outgoing_uri=' + INI.ReadString('konto1','url','');
      TFPCustomHTTPClient.SimpleGet(url_call);
    finally
      Ini.Free;
    end;
end;

procedure TFrmFastdial.BntFastdial9Click(Sender: TObject);
begin
  INI := TINIFile.Create('conf/conf.ini');
    try
      User := INI.ReadString('general','user', '');
      Pass := INI.ReadString('general','pass', '');
      IP := INI.ReadString('general','ip', '');
      Login := User + ':' + Pass + '@' + IP;
      url_call := 'http://' + Login + '/servlet?key=number=' +  INI.ReadString('speedcall','nummer9','') + '&outgoing_uri=' + INI.ReadString('konto1','url','');
      TFPCustomHTTPClient.SimpleGet(url_call);
    finally
      Ini.Free;
    end;
end;

end.

