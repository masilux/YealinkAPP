unit formmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, IpHtml, Ipfilebroker, Forms,
  Controls, Graphics, Dialogs, ComCtrls, ExtCtrls, Menus, DefaultTranslator,
  StdCtrls, formsettings, formabout, lclintf, Buttons, IniFiles,
  fphttpclient, formfastdial, formremotedisplay, functions;

type

  { TFrmMain }

  TFrmMain = class(TForm)
    BtnMicOff: TBitBtn;
    BtnVolumeDown: TBitBtn;
    BtnVolumeUp: TBitBtn;
    BntSpeaker: TBitBtn;
    BtnHeadset: TBitBtn;
    BntTakeoff: TButton;
    BntHangup: TButton;
    GroupBox1: TGroupBox;
    MenuRemoteDisplay: TMenuItem;
    MenuViewfastdial: TMenuItem;
    PmenuFastdial: TMenuItem;
    PmenuSeperator: TMenuItem;
    PmenuSeperator1: TMenuItem;
    EditNumber: TEdit;
    Label1: TLabel;
    MenuMain: TMainMenu;
    PmenuViewmain: TMenuItem;
    MenuHelpindex: TMenuItem;
    MenuHelpSeperator: TMenuItem;
    MenuHelpAbout: TMenuItem;
    PmenuSystemtray: TMenuItem;
    PmenuEndMain: TMenuItem;
    MenuFile: TMenuItem;
    MenuFileend: TMenuItem;
    MenuView: TMenuItem;
    MenuViewsystemtray: TMenuItem;
    MenuFilesettings: TMenuItem;
    MenuHelp: TMenuItem;
    PmntMain1: TPopupMenu;
    TrayIcon1: TTrayIcon;
    procedure BtnMicOffClick(Sender: TObject);
    procedure BtnVolumeDownClick(Sender: TObject);
    procedure BtnVolumeUpClick(Sender: TObject);
    procedure BntSpeakerClick(Sender: TObject);
    procedure BtnHeadsetClick(Sender: TObject);
    procedure BntTakeoffClick(Sender: TObject);
    procedure BntHangupClick(Sender: TObject);
    procedure EditNumberClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure MenuHelpAboutClick(Sender: TObject);
    procedure MenuHelpindexClick(Sender: TObject);
    procedure MenuRemoteDisplayClick(Sender: TObject);
    procedure MenuViewfastdialClick(Sender: TObject);
    procedure PmenuFastdialClick(Sender: TObject);
    procedure PmenuViewmainClick(Sender: TObject);
    procedure PmenuSystemtrayClick(Sender: TObject);
    procedure PmenuEndMainClick(Sender: TObject);
    procedure MenuFileendClick(Sender: TObject);
    procedure MenuViewsystemtrayClick(Sender: TObject);
    procedure MenuFilesettingsClick(Sender: TObject);
  private

  public

  end;

function GetNumerics(s: string): string;

var
  FrmMain: TFrmMain;
  INI:TINIFile;
  User,Pass,IP,Login, iniSystemtray, iniip  : string;
  httpclient: TFPHTTPClient;
  url_call, url_key_x, html, phonenumber : String;
  _userpath: string;

implementation

{$R *.lfm}

{ TFrmMain }
function GetNumerics(s: string): string;
var
  i: Integer;
  tmp_result: string;
begin
  tmp_result := '';

  for i := 1 to Length(s) do
  begin
    if s[i] in ['0'..'9'] then
      tmp_result := tmp_result + s[i];
  end;

  result := tmp_result;
end;

procedure TFrmMain.PmenuViewmainClick(Sender: TObject);
begin
  Show;
end;

procedure TFrmMain.MenuHelpAboutClick(Sender: TObject);
begin
  FrmAbout.Show;
end;

procedure TFrmMain.MenuHelpindexClick(Sender: TObject);
begin
   OpenURL('https://www.masilux.de/doku.php?id=software:yealinkapp:start');
end;

procedure TFrmMain.MenuRemoteDisplayClick(Sender: TObject);
begin
  frmremotedisplay.Show;
end;

procedure TFrmMain.MenuViewfastdialClick(Sender: TObject);
begin
  frmfastdial.Show;
end;

procedure TFrmMain.PmenuFastdialClick(Sender: TObject);
begin
  frmfastdial.Show;
end;

procedure TFrmMain.BntTakeoffClick(Sender: TObject);
begin
    INI := TINIFile.Create(GetUserDir + '.config/yealinkapp.ini');
    try
      phonenumber := GetNumerics(EditNumber.Text);
      User := INI.ReadString('general','user', '');
      Pass := INI.ReadString('general','pass', '');
      IP := INI.ReadString('general','ip', '');
      Login := User + ':' + Pass + '@' + IP;
      url_call := 'http://' + Login + '/servlet?key=number=' +  phonenumber + '&outgoing_uri=' + INI.ReadString('konto1','url','');
      TFPCustomHTTPClient.SimpleGet(url_call);
    finally
      Ini.Free;
    end;
end;

procedure TFrmMain.BtnHeadsetClick(Sender: TObject);
begin
    INI := TINIFile.Create(GetUserDir + '.config/yealinkapp.ini');
    try
      User := INI.ReadString('general','user', '');
      Pass := INI.ReadString('general','pass', '');
      IP := INI.ReadString('general','ip', '');
      Login := User + ':' + Pass + '@' + IP;
      url_key_x := 'http://' + Login + '/servlet?key=HEADSET';
      TFPCustomHTTPClient.SimpleGet(url_key_x);
    finally
      Ini.Free;
    end;
    SendURL(URL('HEADSET',''));
end;

procedure TFrmMain.BtnVolumeDownClick(Sender: TObject);
begin
    INI := TINIFile.Create(GetUserDir + '.config/yealinkapp.ini');
    try
      User := INI.ReadString('general','user', '');
      Pass := INI.ReadString('general','pass', '');
      IP := INI.ReadString('general','ip', '');
      Login := User + ':' + Pass + '@' + IP;
      url_key_x := 'http://' + Login + '/servlet?key=VOLUME_DOWN';
      TFPCustomHTTPClient.SimpleGet(url_key_x);
    finally
      Ini.Free;
    end;
end;

procedure TFrmMain.BtnMicOffClick(Sender: TObject);
begin
    INI := TINIFile.Create(GetUserDir + '.config/yealinkapp.ini');
    try
      User := INI.ReadString('general','user', '');
      Pass := INI.ReadString('general','pass', '');
      IP := INI.ReadString('general','ip', '');
      Login := User + ':' + Pass + '@' + IP;
      url_key_x := 'http://' + Login + '/servlet?key=MUTE';
      TFPCustomHTTPClient.SimpleGet(url_key_x);
    finally
      Ini.Free;
    end;
end;

procedure TFrmMain.BtnVolumeUpClick(Sender: TObject);
begin
    INI := TINIFile.Create(GetUserDir + '.config/yealinkapp.ini');
    try
      User := INI.ReadString('general','user', '');
      Pass := INI.ReadString('general','pass', '');
      IP := INI.ReadString('general','ip', '');
      Login := User + ':' + Pass + '@' + IP;
      url_key_x := 'http://' + Login + '/servlet?key=VOLUME_UP';
      TFPCustomHTTPClient.SimpleGet(url_key_x);
    finally
      Ini.Free;
    end;
end;

procedure TFrmMain.BntSpeakerClick(Sender: TObject);
begin
  INI := TINIFile.Create(GetUserDir + '.config/yealinkapp.ini');
  try
    User := INI.ReadString('general','user', '');
    Pass := INI.ReadString('general','pass', '');
    IP := INI.ReadString('general','ip', '');
    Login := User + ':' + Pass + '@' + IP;
    url_key_x := 'http://' + Login + '/servlet?key=SPEAKER';
    TFPCustomHTTPClient.SimpleGet(url_key_x);
  finally
    Ini.Free;
  end;
end;

procedure TFrmMain.BntHangupClick(Sender: TObject);
begin
    INI := TINIFile.Create(GetUserDir + '.config/yealinkapp.ini');
    try
      User := INI.ReadString('general','user', '');
      Pass := INI.ReadString('general','pass', '');
      IP := INI.ReadString('general','ip', '');
      Login := User + ':' + Pass + '@' + IP;
      url_key_x := 'http://' + Login + '/cgi-bin/ConfigManApp.com?key=X';
      TFPCustomHTTPClient.SimpleGet(url_key_x);
    finally
      Ini.Free;
    end;
end;

procedure TFrmMain.EditNumberClick(Sender: TObject);
begin

end;

procedure TFrmMain.FormActivate(Sender: TObject);
begin
    INI := TINIFile.Create(GetUserDir + '.config/yealinkapp.ini');
    try
      iniSystemtray:=INI.ReadString('general','systemtray', ' ')
    finally
      Ini.Free;
    end;
    if iniSystemtray = '1' then
    begin
      Hide;
    end;
end;

procedure TFrmMain.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  application.Terminate;
end;

procedure TFrmMain.PmenuSystemtrayClick(Sender: TObject);
begin
  Hide;
end;

procedure TFrmMain.PmenuEndMainClick(Sender: TObject);
begin
  application.Terminate;
end;

procedure TFrmMain.MenuFileendClick(Sender: TObject);
begin
  application.Terminate;
end;

procedure TFrmMain.MenuViewsystemtrayClick(Sender: TObject);
begin
  Hide;
end;

procedure TFrmMain.MenuFilesettingsClick(Sender: TObject);
begin
  frmsettings.ShowModal;
end;

end.

