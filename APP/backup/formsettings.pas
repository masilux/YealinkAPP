unit formsettings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ComCtrls, IniFiles;

type

  { TFrmSettings }

  TFrmSettings = class(TForm)
    BtnSave: TButton;
    BtnAbort: TButton;
    ChbSystemtray: TCheckBox;
    EditIP1: TEdit;
    EditKontoname1: TEdit;
    EditKontourl1: TEdit;
    EditNumberfastdial1: TEdit;
    EditNumberfastdial10: TEdit;
    EditNumberfastdial2: TEdit;
    EditNumberfastdial3: TEdit;
    EditNumberfastdial4: TEdit;
    EditNumberfastdial5: TEdit;
    EditNumberfastdial6: TEdit;
    EditNumberfastdial7: TEdit;
    EditNumberfastdial8: TEdit;
    EditNumberfastdial9: TEdit;
    EditPassword1: TEdit;
    EditTextfastdial1: TEdit;
    EditTextfastdial10: TEdit;
    EditTextfastdial2: TEdit;
    EditTextfastdial3: TEdit;
    EditTextfastdial4: TEdit;
    EditTextfastdial5: TEdit;
    EditTextfastdial6: TEdit;
    EditTextfastdial7: TEdit;
    EditTextfastdial8: TEdit;
    EditTextfastdial9: TEdit;
    EditUser1: TEdit;
    Group1: TPageControl;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Konto1: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    LabelSw1: TLabel;
    LabelSw10: TLabel;
    LabelSw2: TLabel;
    LabelSw3: TLabel;
    LabelSw4: TLabel;
    LabelSw5: TLabel;
    LabelSw6: TLabel;
    LabelSw7: TLabel;
    LabelSw8: TLabel;
    LabelSw9: TLabel;
    StaticText1: TStaticText;
    TabFastdial: TTabSheet;
    TabGeneral: TTabSheet;
    TabKonto: TTabSheet;
    procedure BtnSaveClick(Sender: TObject);
    procedure BtnAbortClick(Sender: TObject);
    procedure ChbSystemtrayChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  FrmSettings: TFrmSettings;
  INI:TINIFile;
  ip,user,pass,konto1_name,konto1_url, systemtray:String;

implementation

{$R *.lfm}

{ TFrmSettings }

procedure TFrmSettings.BtnSaveClick(Sender: TObject);
begin
  INI := TINIFile.Create(GetUserDir + '.config/yealinkapp.ini');
  //INI := TINIFile.Create('conf/conf.ini');
  try
    INI.WriteString('general','ip', EditIP1.Text);
    INI.WriteString('general','user', EditUser1.Text);
    INI.WriteString('general','pass', EditPassword1.Text);
    INI.WriteString('konto1','name',EditKontoname1.Text);
    INI.WriteString('konto1','url',EditKontourl1.Text);
    INI.WriteString('speedcall','text1',EditTextfastdial1.Text);
    INI.WriteString('speedcall','nummer1',EditNumberfastdial1.Text);
    INI.WriteString('speedcall','text2',EditTextfastdial2.Text);
    INI.WriteString('speedcall','nummer2',EditNumberfastdial2.Text);
    INI.WriteString('speedcall','text3',EditTextfastdial3.Text);
    INI.WriteString('speedcall','nummer3',EditNumberfastdial3.Text);
    INI.WriteString('speedcall','text4',EditTextfastdial4.Text);
    INI.WriteString('speedcall','nummer4',EditNumberfastdial4.Text);
    INI.WriteString('speedcall','text5',EditTextfastdial5.Text);
    INI.WriteString('speedcall','nummer5',EditNumberfastdial5.Text);
    INI.WriteString('speedcall','text6',EditTextfastdial6.Text);
    INI.WriteString('speedcall','nummer6',EditNumberfastdial6.Text);
    INI.WriteString('speedcall','text7',EditTextfastdial7.Text);
    INI.WriteString('speedcall','nummer7',EditNumberfastdial7.Text);
    INI.WriteString('speedcall','text8',EditTextfastdial8.Text);
    INI.WriteString('speedcall','nummer8',EditNumberfastdial8.Text);
    INI.WriteString('speedcall','text9',EditTextfastdial9.Text);
    INI.WriteString('speedcall','nummer9',EditNumberfastdial9.Text);
    INI.WriteString('speedcall','text10',EditTextfastdial10.Text);
    INI.WriteString('speedcall','nummer10',EditNumberfastdial10.Text);
    INI.WriteBool('general','systemtray',ChbSystemtray.Checked);
  finally
    Ini.Free;
  end;
end;

procedure TFrmSettings.BtnAbortClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmSettings.ChbSystemtrayChange(Sender: TObject);
begin

end;

procedure TFrmSettings.FormCreate(Sender: TObject);
begin
  INI := TINIFile.Create(GetUserDir + '.config/yealinkapp.ini');
  Label6.Caption:= GetUserDir + '.config/yealinkapp.ini';
  //INI := TINIFile.Create('conf/conf.ini');
  try
    EditIP1.Text := INI.ReadString('general','ip', '');
    EditUser1.Text := INI.ReadString('general','user','');
    EditPassword1.Text := INI.ReadString('general','pass','');
    EditKontoname1.Text := INI.ReadString('konto1','name','');
    EditKontourl1.Text := INI.ReadString('konto1','url','');
    EditTextfastdial1.Text:=INI.ReadString('speedcall','text1','');
    EditNumberfastdial1.Text:=INI.ReadString('speedcall','nummer1','');
    EditTextfastdial2.Text:=INI.ReadString('speedcall','text2','');
    EditNumberfastdial2.Text:=INI.ReadString('speedcall','nummer2','');
    EditTextfastdial3.Text:=INI.ReadString('speedcall','text3','');
    EditNumberfastdial3.Text:=INI.ReadString('speedcall','nummer3','');
    EditTextfastdial4.Text:=INI.ReadString('speedcall','text4','');
    EditNumberfastdial4.Text:=INI.ReadString('speedcall','nummer4','');
    EditTextfastdial5.Text:=INI.ReadString('speedcall','text5','');
    EditNumberfastdial5.Text:=INI.ReadString('speedcall','nummer5','');
    EditTextfastdial6.Text:=INI.ReadString('speedcall','text6','');
    EditNumberfastdial6.Text:=INI.ReadString('speedcall','nummer6','');
    EditTextfastdial7.Text:=INI.ReadString('speedcall','text7','');
    EditNumberfastdial7.Text:=INI.ReadString('speedcall','nummer7','');
    EditTextfastdial8.Text:=INI.ReadString('speedcall','text8','');
    EditNumberfastdial8.Text:=INI.ReadString('speedcall','nummer8','');
    EditTextfastdial9.Text:=INI.ReadString('speedcall','text9','');
    EditNumberfastdial9.Text:=INI.ReadString('speedcall','nummer9','');
    EditTextfastdial10.Text:=INI.ReadString('speedcall','text10','');
    EditNumberfastdial10.Text:=INI.ReadString('speedcall','nummer10','');
    ChbSystemtray.Checked:=INI.ReadBool('general','systemtray',False);
  finally
    Ini.Free;
  end;
end;

end.

