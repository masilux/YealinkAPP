unit formsplash;

{$mode objfpc}{$H+}

interface

uses
  Classes, Forms, ExtCtrls, ComCtrls, StdCtrls, formmain, IniFiles,
  formsettings, SysUtils;

type

  { TFrmSplash }

  TFrmSplash = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    ProgressBar1: TProgressBar;
    Timer1: TTimer;
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private

  public

  end;

var
  FrmSplash: TFrmSplash;
  INI:TINIFile;
  iniip  : string;
  _userpath: string;

implementation

{$R *.lfm}

{ TFrmSplash }

procedure TFrmSplash.Timer1Timer(Sender: TObject);
begin
    //_userpath := GetUserDir + '/.config/yealinkapp.ini';
    INI := TINIFile.Create(GetUserDir + '.config/yealinkapp.ini');
    //  INI := TINIFile.Create('conf/conf.ini');
    try
      iniip:=INI.ReadString('general','ip', '')
    finally
      Ini.Free;
    end;

    if iniip = '' then
    begin
      frmsettings.Show;
    end;

    //_userpath := GetUserDir+'/.config/yealinkapp.ini';
    INI := TINIFile.Create(GetUserDir + '.config/yealinkapp.ini');
    //  INI := TINIFile.Create('conf/conf.ini');
    try
      iniip:=INI.ReadString('general','ip', '')
    finally
      Ini.Free;
    end;

    if iniip <> '' then
    begin
      hide;
      FrmMain.ShowModal;
    end;
end;

procedure TFrmSplash.FormShow(Sender: TObject);
begin

end;

end.

