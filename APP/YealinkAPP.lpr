program YealinkAPP;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, runtimetypeinfocontrols, formmain, formsettings, formabout,
  formfastdial, formsplash, formremotedisplay;

{$R *.res}

begin
  Application.Scaled:=True;
  RequireDerivedFormResource:=True;
  Application.Initialize;
  Application.CreateForm(TFrmSplash, FrmSplash);
  Application.CreateForm(Tfrmmain, frmmain);
  Application.CreateForm(Tfrmsettings, frmsettings);
  Application.CreateForm(Tfrmabout, frmabout);
  Application.CreateForm(Tfrmfastdial, frmfastdial);
  Application.CreateForm(TFrmRemoteDisplay, FrmRemoteDisplay);
  Application.Run;
end.

