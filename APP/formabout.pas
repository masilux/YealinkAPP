unit formabout;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  LCLIntf, ExtCtrls, ComCtrls;

type

  { TFrmAbout }

  TFrmAbout = class(TForm)
    BntOk: TButton;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    PageControl1: TPageControl;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    procedure BntOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure Image3Click(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure StaticText1Click(Sender: TObject);
    procedure StaticText3Click(Sender: TObject);
  private

  public

  end;

var
  FrmAbout: TFrmAbout;
  _userpath: string;

implementation

{$R *.lfm}

{ TFrmAbout }

procedure TFrmAbout.BntOkClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmAbout.FormCreate(Sender: TObject);
begin
    _userpath := GetUserDir;
    Label7.Caption := _userpath;
end;

procedure TFrmAbout.Image2Click(Sender: TObject);
begin
  OpenURL('https://www.facebook.com/Masiluxde-509098849508211');
end;

procedure TFrmAbout.Image3Click(Sender: TObject);
begin
  OpenURL('https://www.youtube.com/channel/UCrAO226ks9uQ9Y25smvJ4-g');
end;

procedure TFrmAbout.Label1Click(Sender: TObject);
begin

end;

procedure TFrmAbout.Label2Click(Sender: TObject);
begin

end;

procedure TFrmAbout.PageControl1Change(Sender: TObject);
begin

end;

procedure TFrmAbout.StaticText1Click(Sender: TObject);
begin
  OpenURL('https://www.gnu.org/licenses/gpl');
end;

procedure TFrmAbout.StaticText3Click(Sender: TObject);
begin
  OpenURL('https://www.masilux.de');
end;

end.

